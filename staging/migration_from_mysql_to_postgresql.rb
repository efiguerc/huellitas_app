require 'mysql2'
require 'pg'

def create_default_users

  user = User.new name: "Edgar Figueroa", email: "efiguerc@gmail.com",
                  password: "T3lc3l0racl3", password_confirmation: "T3lc3l0racl3"

  user.activate if user.save

  user = User.new name: "Ana Quijada", email: "anquirod@yahoo.com.mx",
                  password: "C4m1l4C4m1l4", password_confirmation: "C4m1l4C4m1l4"

  user.activate if user.save
end

def migrate_mysql_providers
  m_conn = Mysql2::Client.new( :host     => "localhost",
                             :username => "root",
                             :database => "huellitas00",
                             :password => 'T3lc3l0racl3' )

  m_res = m_conn.query "select * from providers"

  m_res.to_a.each do |r|
    provider = Provider.new name: r["name"], tax_name: r["razon_social"],
                            tax_id: r["rfc"], address1: r["address1"],
                            address2: r["address2"], city: r["city"],
                            state: r["state"], zip: r["zip"],
                            country: r["country"], contact: r["contact"],
                            phone: r["phone"], email: r["email"],
                            webpage: r["webpage"]
    provider.save
  end
end

# p_conn = PG.connect dbname: "huellitas_app_staging"

=begin
rescue Exception => e
  
end
m_res.to_a.each.with_index do |r,i|

  p_conn.exec_params( "insert into providers values
                      ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)",
                      [i, r["name"], r["razon_social"], r["rfc"], r["address1"],
                       r["address2"], r["city"], r["state"], r["zip"], r["country"],
                       r["contact"], nil, r["phone"], r["email"], r["webpage"]])

  puts (r["name"] + "\n")
end
=end

# p_res = p_conn.query "select * from purchases"

