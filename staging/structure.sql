--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: order_detail_sources; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE order_detail_sources (
    id integer NOT NULL,
    quantity integer,
    product_id integer,
    purchase_id integer,
    order_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: order_detail_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_detail_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_detail_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_detail_sources_id_seq OWNED BY order_detail_sources.id;


--
-- Name: order_details; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE order_details (
    id integer NOT NULL,
    quantity integer,
    price numeric,
    product_id integer,
    order_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: order_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE order_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE order_details_id_seq OWNED BY order_details.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE orders (
    id integer NOT NULL,
    ordered_at timestamp without time zone,
    o_providers integer DEFAULT 0,
    o_purchases integer DEFAULT 0,
    items integer DEFAULT 0,
    articles integer DEFAULT 0,
    cost numeric DEFAULT 0.0,
    margin numeric DEFAULT 0.0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    points integer
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    code character varying,
    name character varying,
    description character varying,
    size character varying,
    brand character varying,
    model character varying,
    line character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    cost numeric,
    price numeric,
    quantity integer DEFAULT 0,
    inventory integer DEFAULT 0,
    picture character varying
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: products_purchases; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE products_purchases (
    purchase_id integer,
    product_id integer,
    quantity integer,
    cost numeric
);


--
-- Name: providers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE providers (
    id integer NOT NULL,
    name character varying,
    tax_name character varying,
    tax_id character varying,
    address1 character varying,
    address2 character varying,
    city character varying,
    state character varying,
    zip character varying,
    country character varying,
    contact character varying,
    mobile character varying,
    phone character varying,
    email character varying,
    webpage character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: providers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE providers_id_seq OWNED BY providers.id;


--
-- Name: purchase_details; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE purchase_details (
    quantity integer,
    cost numeric(8,2),
    purchase_id integer,
    product_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    inventory integer
);


--
-- Name: purchases; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE purchases (
    id integer NOT NULL,
    folio character varying,
    purchased_at timestamp without time zone,
    cost numeric DEFAULT 0.0,
    provider_id integer,
    items integer DEFAULT 0,
    articles integer DEFAULT 0,
    difference numeric DEFAULT 0.0,
    inventory integer
);


--
-- Name: purchases_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE purchases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: purchases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE purchases_id_seq OWNED BY purchases.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying,
    email character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_digest character varying,
    admin boolean DEFAULT false,
    activation_digest character varying,
    activated boolean DEFAULT false,
    activated_at timestamp without time zone,
    reset_digest character varying,
    reset_sent_at timestamp without time zone,
    remember_digest character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_detail_sources ALTER COLUMN id SET DEFAULT nextval('order_detail_sources_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_details ALTER COLUMN id SET DEFAULT nextval('order_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY providers ALTER COLUMN id SET DEFAULT nextval('providers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchases ALTER COLUMN id SET DEFAULT nextval('purchases_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: order_detail_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY order_detail_sources
    ADD CONSTRAINT order_detail_sources_pkey PRIMARY KEY (id);


--
-- Name: order_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY order_details
    ADD CONSTRAINT order_details_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: providers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY providers
    ADD CONSTRAINT providers_pkey PRIMARY KEY (id);


--
-- Name: purchases_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY purchases
    ADD CONSTRAINT purchases_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: by_or_pr_pu; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX by_or_pr_pu ON order_detail_sources USING btree (order_id, product_id, purchase_id);


--
-- Name: by_or_pu_pr; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX by_or_pu_pr ON order_detail_sources USING btree (order_id, purchase_id, product_id);


--
-- Name: by_pr_or_pu; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX by_pr_or_pu ON order_detail_sources USING btree (product_id, order_id, purchase_id);


--
-- Name: by_pr_pu_or; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX by_pr_pu_or ON order_detail_sources USING btree (product_id, purchase_id, order_id);


--
-- Name: by_pu_or_pr; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX by_pu_or_pr ON order_detail_sources USING btree (purchase_id, order_id, product_id);


--
-- Name: by_pu_pr_or; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX by_pu_pr_or ON order_detail_sources USING btree (purchase_id, product_id, order_id);


--
-- Name: index_order_detail_sources_on_order_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_order_detail_sources_on_order_id ON order_detail_sources USING btree (order_id);


--
-- Name: index_order_detail_sources_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_order_detail_sources_on_product_id ON order_detail_sources USING btree (product_id);


--
-- Name: index_order_detail_sources_on_purchase_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_order_detail_sources_on_purchase_id ON order_detail_sources USING btree (purchase_id);


--
-- Name: index_order_details_on_order_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_order_details_on_order_id ON order_details USING btree (order_id);


--
-- Name: index_order_details_on_order_id_and_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_order_details_on_order_id_and_product_id ON order_details USING btree (order_id, product_id);


--
-- Name: index_order_details_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_order_details_on_product_id ON order_details USING btree (product_id);


--
-- Name: index_order_details_on_product_id_and_order_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_order_details_on_product_id_and_order_id ON order_details USING btree (product_id, order_id);


--
-- Name: index_products_purchases_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_purchases_on_product_id ON products_purchases USING btree (product_id);


--
-- Name: index_products_purchases_on_product_id_and_purchase_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_products_purchases_on_product_id_and_purchase_id ON products_purchases USING btree (product_id, purchase_id);


--
-- Name: index_products_purchases_on_purchase_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_products_purchases_on_purchase_id ON products_purchases USING btree (purchase_id);


--
-- Name: index_products_purchases_on_purchase_id_and_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_products_purchases_on_purchase_id_and_product_id ON products_purchases USING btree (purchase_id, product_id);


--
-- Name: index_purchase_details_on_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_purchase_details_on_product_id ON purchase_details USING btree (product_id);


--
-- Name: index_purchase_details_on_product_id_and_purchase_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_purchase_details_on_product_id_and_purchase_id ON purchase_details USING btree (product_id, purchase_id);


--
-- Name: index_purchase_details_on_purchase_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_purchase_details_on_purchase_id ON purchase_details USING btree (purchase_id);


--
-- Name: index_purchase_details_on_purchase_id_and_product_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_purchase_details_on_purchase_id_and_product_id ON purchase_details USING btree (purchase_id, product_id);


--
-- Name: index_purchases_on_provider_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_purchases_on_provider_id ON purchases USING btree (provider_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_2bd512e915; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_detail_sources
    ADD CONSTRAINT fk_rails_2bd512e915 FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- Name: fk_rails_4f2ac9473b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_details
    ADD CONSTRAINT fk_rails_4f2ac9473b FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: fk_rails_58df8cd8f8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_detail_sources
    ADD CONSTRAINT fk_rails_58df8cd8f8 FOREIGN KEY (purchase_id) REFERENCES purchases(id);


--
-- Name: fk_rails_936a28db19; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchase_details
    ADD CONSTRAINT fk_rails_936a28db19 FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: fk_rails_956f4552b0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_detail_sources
    ADD CONSTRAINT fk_rails_956f4552b0 FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: fk_rails_9d96285b8e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY purchase_details
    ADD CONSTRAINT fk_rails_9d96285b8e FOREIGN KEY (purchase_id) REFERENCES purchases(id);


--
-- Name: fk_rails_e5976611fd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY order_details
    ADD CONSTRAINT fk_rails_e5976611fd FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20141028143035');

INSERT INTO schema_migrations (version) VALUES ('20141028160250');

INSERT INTO schema_migrations (version) VALUES ('20141028164255');

INSERT INTO schema_migrations (version) VALUES ('20141029055551');

INSERT INTO schema_migrations (version) VALUES ('20141030192332');

INSERT INTO schema_migrations (version) VALUES ('20141031002525');

INSERT INTO schema_migrations (version) VALUES ('20141031033420');

INSERT INTO schema_migrations (version) VALUES ('20141105164446');

INSERT INTO schema_migrations (version) VALUES ('20141113152444');

INSERT INTO schema_migrations (version) VALUES ('20150520231439');

INSERT INTO schema_migrations (version) VALUES ('20150521220429');

INSERT INTO schema_migrations (version) VALUES ('20150523222257');

INSERT INTO schema_migrations (version) VALUES ('20150528234540');

INSERT INTO schema_migrations (version) VALUES ('20150612052345');

INSERT INTO schema_migrations (version) VALUES ('20150616222223');

INSERT INTO schema_migrations (version) VALUES ('20150617201341');

INSERT INTO schema_migrations (version) VALUES ('20150617211110');

INSERT INTO schema_migrations (version) VALUES ('20150619234814');

INSERT INTO schema_migrations (version) VALUES ('20150623234737');

INSERT INTO schema_migrations (version) VALUES ('20150624002416');

INSERT INTO schema_migrations (version) VALUES ('20150625185822');

INSERT INTO schema_migrations (version) VALUES ('20150704203034');

INSERT INTO schema_migrations (version) VALUES ('20150711192155');

INSERT INTO schema_migrations (version) VALUES ('20150711192948');

INSERT INTO schema_migrations (version) VALUES ('20150711205112');

INSERT INTO schema_migrations (version) VALUES ('20150712060700');

