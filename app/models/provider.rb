class Provider < ActiveRecord::Base
  has_many :purchases
	before_save { self.email = email.downcase if self.email }

	validates :name,  presence: true, length: { maximum: 50 },
										uniqueness: true
	validates :tax_name, allow_blank: true, length: { maximum: 100 },
											 uniqueness: true
	validates :tax_id, allow_blank: true, length: { maximum: 30 },
										 uniqueness: true
	validates :address1, allow_blank: true, length: { maximum: 50 }
	validates :address2, allow_blank: true, length: { maximum: 50 }
	validates :city, allow_blank: true, length: { maximum: 50 }
	validates :state, allow_blank: true, length: { maximum: 50 }
	validates :zip, allow_blank: true, length: { maximum: 5 }
	validates :country, allow_blank: true, length: { maximum: 50 }
	validates :contact, allow_blank: true, uniqueness: true, length: { maximum: 50 }
	validates :mobile, allow_blank: true, uniqueness: true, length: { maximum: 30 }
	validates :phone, allow_blank: true, uniqueness: true, length: { maximum: 30 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, allow_blank: true, length: { maximum: 255 },
  									format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :webpage, allow_blank: true, length: { maximum: 255 },
  										uniqueness: true
end
