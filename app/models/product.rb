class Product < ActiveRecord::Base
  attr_reader :margin, :percent
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  has_many :purchase_detail
  has_many :purchases, through: :purchase_detail
  has_many :order_detail
  has_many :orders, through: :order_detail
  has_many :providers, through: :purchases

  mount_uploader :picture, PictureUploader

  after_create :enqueue_picture
  after_update :crop_picture

	validates :code, length: { maximum: 40 }, uniqueness: true
	validates :name,  presence: true, length: { maximum: 100 },
										uniqueness: {scope: :size}
	validates :description,  presence: true, length: { maximum: 255 }
	validates :size,  presence: true, length: { maximum: 20 },
										uniqueness: {scope: :name}
	validates :brand, length: { maximum: 100 }
	validates :model, allow_blank: true, length: { maximum: 100 }, uniqueness: true
	validates :line, length: { maximum: 100 }
  validate  :picture_size

  def margin
    self.price - self.cost if self.price && self.cost
  end

  def percent
    self.margin / self.price if self.margin
  end


  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

    def crop_picture
      if self.crop_x.present?
        self.picture.crop
        self.picture.resize_to_limit(1024, 1024)
        # picture.filename =~ /\/([^\/]*\.[^\.]*)[\?$]/
        # self.key = $1
        self.crop_x = nil
        self.picture.recreate_versions!
        self.remote_picture_url = picture.direct_fog_url(with_path: true)
        self.save!
        # enqueue_picture
      end
    end

    def enqueue_picture
      # PictureWorker.perform_async(id, key) unless key.index("filename")
      unless key.index("filename")
        self.key = key
        self.remote_picture_url = picture.direct_fog_url(with_path: true)
        self.save!
      end
    end

    class PictureWorker
      include Sidekiq::Worker

      def perform(id, key)
        product = Product.find id
        product.key = key
        product.remote_picture_url = product.picture.direct_fog_url(with_path: true)
        product.save!
      end
    end
end
