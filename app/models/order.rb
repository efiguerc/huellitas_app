class Order < ActiveRecord::Base
  attr_reader :total
  has_many :order_detail
  has_many :products, through: :order_detail
  accepts_nested_attributes_for :order_detail

  before_validation :fix_currency_fields

  def total
    sum = 0
    self.order_detail.each do |od|
      sum += od.quantity * od.price
    end
    sum
  end

  private

    def fix_currency_fields
      self[:cost] = cost_before_type_cast.tr('$ ,','')
      self[:margin] = margin_before_type_cast.tr('$ ,','')
    end
end
