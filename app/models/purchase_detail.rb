class PurchaseDetail < ActiveRecord::Base
  belongs_to :purchase
  belongs_to :product

  before_validation :update_purchase_detail
  before_save       :update_product

  #validates  :purchase_id, presence: true
  validates  :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates  :product_id, presence: true, uniqueness: { scope: :purchase_id }
  validates  :cost, presence: true, numericality: { greater_than: 0 }

  private

    def update_purchase_detail
      self.inventory = self.quantity
    end

    def update_product
      product = Product.find product_id
      product.cost = cost
      product.quantity += quantity
      product.inventory += quantity
      product.p_providers += 1 unless product.providers.exists? purchase.provider
      product.save
    end
end
