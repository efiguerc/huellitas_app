class OrderDetail < ActiveRecord::Base
  belongs_to :product
  belongs_to :order

  after_save :update_product

  private

    def update_product
      product = Product.find product_id
      product.update_attributes price: price,
                                inventory: (product.inventory - quantity)
    end
end
