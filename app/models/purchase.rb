class Purchase < ActiveRecord::Base
  attr_reader :total
  belongs_to :provider
  has_many :purchase_detail
  has_many :products, through: :purchase_detail
  accepts_nested_attributes_for :purchase_detail

  before_validation :fix_currency_fields
  before_validation :setup_inventory

  validates  :provider_id, presence: true
  validates  :folio, presence: true, uniqueness: true
  validates  :purchased_at, presence: true
  validates  :cost, presence: true, numericality: { greater_than: 0 }
  validates  :items, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates  :articles, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates  :difference, presence: true, numericality: true
  validates  :purchase_detail, presence: true
  validate   :detail_uniqueness
  validates_associated :purchase_detail

  def total
    sum = 0
    self.purchase_detail.each do |pd|
      sum += pd.quantity * pd.cost
    end
    sum
  end

  private

    def fix_currency_fields
      self[:cost] = cost_before_type_cast.tr('$ ,','')
      self[:difference] = difference_before_type_cast.tr('$ ,','')
    end

    def setup_inventory
      self[:inventory] = self[:quantity]
    end

    def detail_uniqueness
      return if self.id
      hash = {}
      self.purchase_detail.each do |record|
        if hash[record.product_id]
          # This line is needed to form the parent to error out, otherwise the save would still happen
          self.errors[:"product_id #{record.product_id}"] << "duplicated: #{record.product.description}" if self.errors[:"product_id #{record.product_id}"].blank?
          # This line adds the error to the child to view in your fields_for
          record.errors[:product_id] << "has already been taken"
        end
        hash[record.product_id] = true
      end
    end
end