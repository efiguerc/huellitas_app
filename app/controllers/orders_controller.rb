class OrdersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :new, :create, :edit, :update, :destroy]
  before_action :admin_user,     only: [:edit, :update, :destroy]

  def index
    @orders = Order.all
  end

  def show
    @order = Order.find(params[:id])
  end

  def new
    @order = Order.new
    @products = Product.all
  end

  def create
    @order = Order.new(order_params)
    if @order.save
      flash[:notice] = "Orden registrada."
      redirect_to :action => :index
    else
      render 'new'
    end
  end

  private

    def order_params
      params.require(:order).permit(:ordered_at, :items, :articles,
                                    :cost, :margin, :points).tap do |w|
        if params[:order][:order_detail_attributes]
          w[:order_detail_attributes] = params[:order][:order_detail_attributes]
        end
      end
    end


    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
