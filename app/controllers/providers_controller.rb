class ProvidersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :new, :create, :edit, :update, :destroy]
  before_action :admin_user,     only: [:edit, :update, :destroy]

	def index
    @providers = Provider.paginate(page: params[:page])
  end

  def show
  	@provider = Provider.find(params[:id])
  end

  def new
  	@provider = Provider.new
  end

  def create
    @provider = Provider.new(provider_params)    # Not the final implementation!
    if @provider.save
    	flash[:success] = "Proveedor Creado."
      redirect_to @provider
    else
      render 'new'
    end
  end

  def edit
    @provider = Provider.find(params[:id])
  end

  def update
    @provider = Provider.find(params[:id])
    if @provider.update_attributes(provider_params)
      flash[:success] = "Proveedor Actualizado."
      redirect_to @provider
    else
      render 'edit'
    end
  end

  def destroy
    Provider.find(params[:id]).destroy
    flash[:success] = "Proveedor Eliminado."
    redirect_to providers_url
  end

  private

    def provider_params
      params.require(:provider).permit(:name, :tax_name, :tax_id, :address1,
                                   		 :address2, :city, :state, :zip,
                                   		 :country, :contact, :mobile, :phone,
                                   		 :email, :webpage)
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
