class ProductsController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :new, :create, :edit, :update, :destroy]
  before_action :admin_user,     only: [:edit, :destroy]

  def index
    #@products = Product.paginate(page: params[:page])
    @products = Product.all
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new(key: params[:key])
    @uploader = @product.picture
    @uploader.success_action_redirect = new_product_url
  end

  def create
    @product = Product.new(product_params)    # Not the final implementation!
    if @product.save
      unless product_params[:key].index("filename")
        redirect_to crop_url id: @product.id
      else
        flash[:success] = "Producto Creado."
        redirect_to @product
      end
    else
      render 'new'
    end
  end

  def crop
    @product = Product.find(params[:id])
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(product_params)
      if params[:product][:picture].present?
        render :crop
      else
        flash[:success] = "Producto Actualizado."
        redirect_to @product
      end
    else
      render 'edit'
    end
  end

  def destroy
    Product.find(params[:id]).destroy
    flash[:success] = "Producto Eliminado."
    redirect_to products_url
  end


  private

    def product_params
      params.require(:product).permit(:key, :code, :name, :description, :size,
                                      :brand, :model, :line, :picture,
                                      :crop_x, :crop_y, :crop_w, :crop_h)
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
