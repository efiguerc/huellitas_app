class PurchasesController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :new, :create, :edit, :update, :destroy]
  before_action :admin_user,     only: [:edit, :update, :destroy]

  def index
    #@purchases = Purchase.paginate(page: params[:page])
    @purchases = Purchase.all
  end

  def show
    @purchase = Purchase.find(params[:id])
  end

  def new
    @purchase = Purchase.new
    @products = Product.all
  end

  def create
    @purchase = Purchase.new(purchase_params)
    if @purchase.save
      flash[:notice] = "Compra registrada."
      redirect_to :action => :index
    else
      @products = Product.all
      render 'new'
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

    private

    def purchase_params
      params.require(:purchase).permit(:provider_id, :folio,
                                       :purchased_at, :cost, :items, :articles, :difference).tap do |w|
        if params[:purchase][:purchase_detail_attributes]
          w[:purchase_detail_attributes] = params[:purchase][:purchase_detail_attributes]
        end
      end
    end


    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end