class NewOrder
  constructor: ->
    @bindEvents()
    $('#orders').dataTable()
    $('#order_details').dataTable
      "paging":         false
    $('#order_ordered_at').datepicker
      dateFormat: 'dd/mm/yy'

  bindEvents: ->
    $('form').on 'click', '.order_product_detail_row', @addRow
    $('form').on 'click', '.order_remove_fields', @removeFields
    $('.money').focusout @formatMoney

  formatMoney: ->
    $(this).formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})

  addRow: (event) =>
    @cargar = $(event.target).parent()
    @computeAddData()
    @updateForm()
    @updateTable()
    $('#order_detail_quantity').focus()
    event.preventDefault()

  computeAddData: ->
    @quantity = $('#order_detail_quantity').val()
    @price = $('#order_detail_price').asNumber()
    @product_id = $(@cargar).find('.id').val()
    @product = $(@cargar).find('.description').text()
    @subtotal = @quantity * @price
    @index = parseInt($('#index').val()) + 1
    @items = parseInt($('#order_items').val()) + 1
    @articles = parseInt($('#order_articles').val()) + parseInt(@quantity)
    @total = $('#total').asNumber() + @subtotal
    @cost_subtotal = $(@cargar).find('.cost').asNumber() * @quantity
    @cost = $('#order_cost').asNumber() + @cost_subtotal
    @margin = @total - @cost
    @points = parseInt((@total - @cost) * 100 / @total)

  updateForm: ->
    $('#order_items').val(@items)
    $('#order_articles').val(@articles)
    $('#order_cost').val(@cost)
    $('#order_cost').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    $('#order_margin').val(@margin)
    $('#order_margin').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    $('#order_points').val(@points)
    $('#total').text(@total)
    $('#total').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    $('#index').val(@index)

  updateTable: ->
    @insertRow()
    @updateRowFields()
    @updateRowView()

  insertRow: ->
    new_row = @setRowUniqueID()
    $('#total_row').before(new_row)
    @$new_row = $('#total_row').prev()

  setRowUniqueID: ->
    @time = new Date().getTime()
    regexp = new RegExp($('#order_detail_template').data('id'), 'g')
    $('#order_detail_template').data('fields').replace(regexp, @time)

  updateRowFields: ->
    $('#order_order_detail_attributes_' + @time + '_quantity').val(@quantity)
    $('#order_order_detail_attributes_' + @time + '_product_id').val(@product_id)
    $('#order_order_detail_attributes_' + @time + '_price').val(@price)
    $(@$new_row).find('.cost_subtotal').val(@cost_subtotal)

  updateRowView: ->
    @$new_row.find('.index').text(@index)
    @$new_row.find('.quantity').text(@quantity)
    @$new_row.find('.product').text(@product)
    @$new_row.find('.price').text(@price)
    @$new_row.find('.price').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    @$new_row.find('.subtotal').text(@subtotal)
    @$new_row.find('.subtotal').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})

  removeFields: (event) =>
    @remove = event.target
    @computeRemoveData()
    @updateForm()
    @adjustTableIndex()
    @row.remove()
    event.preventDefault()

  computeRemoveData: ->
    @quantity = $(@remove).parent().prev().prev().prev().prev().asNumber()
    @subtotal = $(@remove).parent().prev().asNumber()
    @items = $('#order_items').val() - 1
    @articles = $('#order_articles').val() - @quantity
    @total = $('#total').asNumber() - @subtotal
    @index = $('#index').val() - 1 
    @cost = $('#order_cost').asNumber() - $(@remove).parent().parent().find('.cost_subtotal').val()
    @margin = @total - @cost
    @points = parseInt((@total - @cost) * 100 / @total)

  adjustTableIndex: ->
    @row = $(@remove).parent().parent()
    @row.nextAll().find("td:first-child").each ->
      index = $(this).text()
      if( index != "")
        $(this).text(index - 1)

$ ->
  app = new NewOrder()
