class NewPurchase
  constructor: ->
    @bindEvents()
    $('#purchases').dataTable()
    $('#purchase_details').dataTable
      "paging":         false
    $('#purchase_purchased_at').datepicker
      dateFormat: 'dd/mm/yy'
    $('#products_index').dataTable
      "scrollY":        "80px"
      "scrollCollapse": false
      "paging":         false

  bindEvents: ->
    $('form').on 'click', '.product_detail_row', @addRow
    $('form').on 'click', '.remove_fields', @removeFields
    $('.money').focusout @formatMoney
    $('#purchase_cost').focusout  @updateDifference

  updateDifference: ->
    total = $('#total').asNumber()
    cost = $('#purchase_cost').asNumber()
    difference = total - cost
    $('#purchase_difference').val(difference)
    $('#purchase_difference').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})

  formatMoney: ->
    $(this).formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})

  addRow: (event) =>
    @cargar = $(event.target).parent()
    @computeAddData()
    @updateForm()
    @updateTable()
    $('#purchase_detail_quantity').focus()
    event.preventDefault()

  computeAddData: ->
    @quantity = $('#purchase_detail_quantity').val()
    @cost = $('#purchase_detail_cost').asNumber()
    @product_id = $(@cargar).find('.id').val()
    @product = $(@cargar).find('.description').text()
    @subtotal = @quantity * @cost
    @index = parseInt($('#index').val()) + 1
    @items = parseInt($('#purchase_items').val()) + 1
    @articles = parseInt($('#purchase_articles').val()) + parseInt(@quantity)
    @total = $('#total').asNumber() + @subtotal
    @difference = @total - $('#purchase_cost').asNumber()

  updateForm: ->
    $('#purchase_items').val(@items)
    $('#purchase_articles').val(@articles)
    $('#total').text(@total)
    $('#total').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    $('#purchase_difference').val(@difference)
    $('#purchase_difference').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    $('#index').val(@index)

  updateTable: ->
    @insertRow()
    @updateRowFields()
    @updateRowView()

  insertRow: ->
    new_row = @setRowUniqueID()
    $('#total_row').before(new_row)
    @$new_row = $('#total_row').prev()

  setRowUniqueID: ->
    @time = new Date().getTime()
    regexp = new RegExp($('#purchase_detail_template').data('id'), 'g')
    $('#purchase_detail_template').data('fields').replace(regexp, @time)

  updateRowFields: ->
    $('#purchase_purchase_detail_attributes_' + @time + '_quantity').val(@quantity)
    $('#purchase_purchase_detail_attributes_' + @time + '_product_id').val(@product_id)
    $('#purchase_purchase_detail_attributes_' + @time + '_cost').val(@cost)

  updateRowView: ->
    @$new_row.find('.index').text(@index)
    @$new_row.find('.quantity').text(@quantity)
    @$new_row.find('.product').text(@product)
    @$new_row.find('.cost').text(@cost)
    @$new_row.find('.cost').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})
    @$new_row.find('.subtotal').text(@subtotal)
    @$new_row.find('.subtotal').formatCurrency({negativeFormat: '-%s %n', positiveFormat: '%s %n', colorize: true})

  removeFields: (event) =>
    @remove = event.target
    @computeRemoveData()
    @updateForm()
    @adjustTableIndex()
    @row.remove()
    event.preventDefault()

  computeRemoveData: ->
    @quantity = $(@remove).parent().prev().prev().prev().prev().asNumber()
    @subtotal = $(@remove).parent().prev().asNumber()
    @items = $('#purchase_items').val() - 1
    @articles = $('#purchase_articles').val() - @quantity
    @total = $('#total').asNumber() - @subtotal
    @difference = @total - $('#purchase_cost').asNumber()
    @index = $('#index').val() - 1

  adjustTableIndex: ->
    @row = $(@remove).parent().parent()
    @row.nextAll().find("td:first-child").each ->
      index = $(this).text()
      if( index != "")
        $(this).text(index - 1)

$ ->
  app = new NewPurchase()