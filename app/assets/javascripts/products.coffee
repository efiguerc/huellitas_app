# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

class NewProduct
  constructor: ->
    @bindEvents()
    $('#products').dataTable
      "scrollY":        "470px"
      "scrollCollapse": false
      "paging":         false
    $('#cropbox').Jcrop
      onSelect: @update
      onChange: @update

  bindEvents: ->
    $('#product_picture').change @validatePicture

  update: (coords) =>
    $('#product_crop_x').val(coords.x)
    $('#product_crop_y').val(coords.y)
    $('#product_crop_w').val(coords.w)
    $('#product_crop_h').val(coords.h)
    @updatePreview(coords)

  updatePreview: (coords) =>
    if coords.w > coords.h
      factor = 100/coords.w
    else
      factor = 100/coords.h
    $('#preview_port').css
      width:  Math.round(factor * coords.w) + 'px'
      height:  Math.round(factor * coords.h) + 'px'
    $('#preview').css
      width: Math.round(factor * $('#cropbox').width()) + 'px'
      height: Math.round(factor * $('#cropbox').height()) + 'px'
      marginLeft: '-' + Math.round(factor * coords.x) + 'px'
      marginTop: '-' + Math.round(factor * coords.y) + 'px'

  validatePicture: ->
    size_in_megabytes = this.files[0].size/1024/1024;
    if (size_in_megabytes > 5)
      alert('Maximum file size is 5MB. Please choose a smaller file.');

$ ->
  app = new NewProduct()