root = "/var/www/huellitas_app/current"
directory root
pidfile "#{root}/tmp/pids/puma.pid"
stdout_redirect "#{root}/log/puma.log", "#{root}/log/puma.log", true

bind "unix://#{root}/tmp/sockets/puma.huellitas_app.sock"
workers 2
worker_timeout 30

preload_app!

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end
