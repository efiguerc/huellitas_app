# config valid only for current version of Capistrano
lock '3.4.0'

set :user, 'deployer'

set :application, 'huellitas_app'
set :scm, :git
set :repo_url, "git@bitbucket.org:efiguerc/#{fetch(:application)}.git"
set :branch, :master
#ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :deploy_to, "/var/www/#{fetch(:application)}"
set :pty, true

set :linked_files, fetch(:linked_files, []).push(
        'config/database.yml',
        'config/secrets.yml',
        'config/application.yml' )

set :linked_dirs, fetch(:linked_dirs, []).push(
        'log',
        'tmp/pids',
        'tmp/cache',
        'tmp/sockets',
        'vendor/bundle',
        'config/.certs' )

set :keep_releases, 5

set :rbenv_type, :user
set :rbenv_ruby, '2.3.0'

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

namespace :deploy do

  %w[start stop restart].each do |command|
    desc "#{command} puma server"
    task command.intern do
      on roles(:app), in: :sequence, wait: 5 do
        execute "sudo puma_#{fetch(:application)} #{command}"
      end
    end
  end

  desc "Sets up the configuration needed to deploy the first time."
  task :setup_config do
    on roles(:app) do
      execute "mkdir -p #{fetch(:deploy_to)}/current/config #{shared_path}/config/.certs"
      upload! 'config/puma_init.sh', "#{fetch(:deploy_to)}/current/config/puma_init.sh"
      sudo "ln -nfs #{fetch(:deploy_to)}/current/config/puma_init.sh /etc/init.d/puma_#{fetch(:application)}"
      sudo "update-rc.d puma_#{fetch(:application)} defaults"
      upload! 'config/nginx.conf', "#{fetch(:deploy_to)}/current/config/nginx.conf"
      sudo "ln -nfs #{fetch(:deploy_to)}/current/config/nginx.conf /etc/nginx/sites-enabled/#{fetch(:application)}"
      sudo "service nginx restart"
      execute "rm -Rf #{fetch(:deploy_to)}/current"
      upload! 'config/database.yml', "#{shared_path}/config/database.yml"
      upload! 'config/secrets.yml', "#{shared_path}/config/secrets.yml"
      upload! 'config/application.yml', "#{shared_path}/config/application.yml"
      upload! 'config/server.crt', "#{shared_path}/config/.certs/server.crt"
      upload! 'config/server.key', "#{shared_path}/config/.certs/server.key"
      warn "Now edit your 'config/' files in #{shared_path}."
    end
  end

  desc "Create new db from schema."
  task :reset_db do
    on primary fetch(:migration_role) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:drop db:create db:schema:load'
        end
      end
    end
  end
  # before :updated, :reset_db

  desc "Make sure the puma server is started."
  after :publishing, :start_puma do
    on roles(:app) do
      sudo "service puma_#{fetch(:application)} stop"
      sudo "service puma_#{fetch(:application)} start"
      sudo "service nginx reload"
    end
  end

  desc "Make sure local git is in sync with remote."
  before :deploy, :check_revision do
    on roles(:web) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        warn "HEAD is not the same as origin/master: Run `git push` to sync changes."
        exit
      end
    end
  end
end
