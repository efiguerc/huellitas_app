require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "Huellitas Web App"
    assert_equal full_title("Help"), "Help | Huellitas Web App"
  end
end