require 'test_helper'

class ProvidersControllerTest < ActionController::TestCase


  #  was the web request successful?
  #  was the user redirected to the right page?
  #  was the user successfully authenticated?
  #  was the correct object stored in the response template?
  #  was the appropriate message displayed to the user in the view?

  # Routing

  test "should Route correctly" do
  	assert_routing '/providers', controller: "providers", action: "index"
  	assert_routing({ method: 'get', path: '/providers/1' },
  								 { controller: "providers", action: "show", id: "1" })
  	assert_routing '/providers/new', controller: "providers", action: "new"
  	assert_routing({ method: 'post', path: '/providers' },
  								 { controller: "providers", action: "create" })
  	assert_routing '/providers/1/edit',
  									 controller: "providers", action: "edit", id: "1"
  	assert_routing({ method: 'patch', path: '/providers/1' },
  								 { controller: "providers", action: "update", id: "1" })
  	assert_routing({ method: 'put', path: '/providers/1' },
  								 { controller: "providers", action: "update", id: "1" })
  	assert_routing({ method: 'delete', path: '/providers/1' },
  								 { controller: "providers", action: "destroy", id: "1" })
  end

  # Not Signed In 

  test "when Not Signed In should not get Index" do
    get :index
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], providers_url, "providers_url not stored!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  test "when Not Signed In should not get Show" do
  	@provider = providers(:lupi)
    get :show, id: @provider.id
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], provider_url(@provider), "provider_url not stored!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  test "when Not Signed In should not get New" do
    get :new
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], new_provider_url, "new_provider_url not stored!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  test "when Not Signed In should not post Create" do
  	assert_no_difference 'Provider.count' do
    	post :create
  	end
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], nil, "session[:forwarding_url] not nil!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  test "when Not Signed In should not get Edit" do
  	@provider = providers(:lupi)
    get :edit, id: @provider.id
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], edit_provider_url(@provider), "edit_provider_url not nil!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  test "when Not Signed In should not patch Update" do
  	@provider = providers(:lupi)
    patch :update, id: @provider.id
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], nil, "session[:forwarding_url] not nil!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  test "when Not Signed In should not delete Destroy" do
  	@provider = providers(:lupi)
  	assert_no_difference 'Provider.count' do
    	delete :destroy, id: @provider.id
  	end
  	assert_not_nil Provider.find_by id: @provider
    assert_response :redirect, "No redirection!"
    assert_redirected_to login_url, 'Not redirected to "login_url"!'
    assert_equal session[:forwarding_url], nil, "session[:forwarding_url] not nil!"
  	assert_equal flash[:danger], "Please log in."
  	assert_template
  end

  # Not Signed In 

	test "when Signed In should get Index" do
		@user = users(:archer)
  	log_in_as(@user)
    get :index
    assert_response :success, "No Success response!"
    assert_template layout: 'application' 
    assert_template layout: "layouts/application", partial: "_shim"
    assert_template layout: "layouts/application", partial: "_header"
    assert_template layout: "layouts/application", partial: "_footer"
    assert_template :index
    assert_template partial: "_provider"
  end

  test "when Signed In should get Show" do
  	@user = users(:archer)
  	log_in_as(@user)
  	@provider = providers(:lupi)
    get :show, id: @provider.id
    assert_response :success, "No Success response!"
    assert_template layout: 'application' 
    assert_template layout: "layouts/application", partial: "_shim"
    assert_template layout: "layouts/application", partial: "_header"
    assert_template layout: "layouts/application", partial: "_footer"
    assert_template :show
  end

  test "when Signed In should get New" do
  	@user = users(:archer)
  	log_in_as(@user)
    get :new
    assert_response :success, "No Success response!"
    assert_template layout: 'application' 
    assert_template layout: "layouts/application", partial: "_shim"
    assert_template layout: "layouts/application", partial: "_header"
    assert_template layout: "layouts/application", partial: "_footer"
    assert_template :new
    assert_template partial: "_fields"
  end

  test "when Signed In should post Create" do
  	@user = users(:archer)
  	log_in_as(@user)
  	assert_difference 'Provider.count', 1 do
    post :create, provider: { name:     'PET PLANET',
		  												tax_name: 'PET PLANET ACUARIUM',
														  tax_id: 	'MAQ8009053C5',
														  address1: 'RIO FRIO  No 174',
														  address2: 'MAGDALENA MIXHUCA',
														  city:     'VENUSTIANO CARRANZA',
														  state:    'DISTRITO FEDERAL',
														  zip:      '15850',
														  country:  'MÉXICO',
														  contact:  'VIRY',
														  #mobile: 	'',
														  phone:    '26123769',
														  email: 		'',
														  webpage: 	'' }
		end
		assert_not_nil Provider.find_by name: 'PET PLANET'
		assert_response :redirect, "should be Redirect!"
		assert_redirected_to provider_url(id: '1061157989'),'Shold redirect to "provider_url"!'
		assert_equal flash[:success], "Proveedor Creado."
    assert_template
  end

	test "when Signed In as Regular User should not get Edit" do
  	@user = users(:archer)
  	log_in_as(@user)
  	@provider = providers(:lupi)
    get :edit, id: @provider.id
    assert_response :redirect, "Response should be Redirect!"
    assert_redirected_to root_url, 'Shold redirect to "root_url"!'
    assert_template
  end

  test "when Signed In as Regular User should not patch Update" do
  	@user = users(:archer)
  	log_in_as(@user)
  	@provider = providers(:lupi)
    patch :update, id: @provider.id
    assert_response :redirect, "Response should be Redirect!"
    assert_redirected_to root_url, 'Shold redirect to "root_url"!'
    assert_template
  end

  test "when Signed In as Regular User should not delete Destroy" do
  	@user = users(:archer)
  	log_in_as(@user)
  	@provider = providers(:lupi)
  	assert_no_difference 'Provider.count' do
    	delete :destroy, id: @provider.id
  	end
  	assert_not_nil Provider.find_by id: @provider.id
    assert_response :redirect, "Response should be Redirect!"
    assert_redirected_to root_url, 'Shold redirect to "root_url"!'
    assert_template
  end

  test "when Signed In as Admin User should get Edit" do
  	@user = users(:michael)
  	log_in_as(@user)
  	@provider = providers(:lupi)
    get :edit, id: @provider.id
    assert_response :success, "No Success response!"
    assert_template layout: 'application' 
    assert_template layout: "layouts/application", partial: "_shim"
    assert_template layout: "layouts/application", partial: "_header"
    assert_template layout: "layouts/application", partial: "_footer"
    assert_template :edit
    assert_template partial: "_fields"
  end

  test "when Signed In as Admin User should patch Update" do
  	@user = users(:michael)
  	log_in_as(@user)
  	@provider = providers(:lupi)
    patch :update, id: @provider, provider: { mobile: '5578765432'}
    assert_response :redirect, "Response should be Redirect!"
		assert_redirected_to provider_url(@provider), 'Shold redirect to "provider_url"!'
		assert_equal flash[:success], "Proveedor Actualizado."
    assert_template
    @provider.reload
    assert_equal @provider.mobile, '5578765432'
  end

  test "when Signed In as Admin User should delete Destroy" do
  	@user = users(:michael)
  	log_in_as(@user)
  	@provider = providers(:ray)
  	assert_difference 'Provider.count', -1 do
	    delete :destroy, id: @provider
	  end
	  assert_nil Provider.find_by id: @provider.id
    assert_response :redirect, "Response should be Redirect!"
		assert_redirected_to providers_url, 'Shold redirect to "providers_url"!'
		assert_equal flash[:success], "Proveedor Eliminado."
    assert_template
  end

  test "when Signed In with Invalid Parameters should not post Create" do
  	@user = users(:michael)
  	log_in_as(@user)
  	assert_no_difference 'Provider.count' do
    	post :create, provider: { name: '', tax_id: "TAX435467ID1" }
		end
		assert_nil Provider.find_by tax_id: "TAX435467ID1"
		assert_response :success, "No Success response!"
    assert_template layout: 'application' 
    assert_template layout: "layouts/application", partial: "_shim"
    assert_template layout: "layouts/application", partial: "_header"
    assert_template layout: "layouts/application", partial: "_footer"
    assert_template :new
    assert_template partial: "_fields"
    assert_template partial: "_error_messages"
  end

  test "when Signed In as Admin User with Invalid Prameters should not patch Update" do
  	@user = users(:michael)
  	log_in_as(@user)
  	@provider = providers(:lupi)
    patch :update, id: @provider, provider: { name: ''}
    assert_response :success, "No Success response!"
    assert_template layout: 'application' 
    assert_template layout: "layouts/application", partial: "_shim"
    assert_template layout: "layouts/application", partial: "_header"
    assert_template layout: "layouts/application", partial: "_footer"
    assert_template :edit
    assert_template partial: "_fields"
    assert_template partial: "_error_messages"
  end
end
