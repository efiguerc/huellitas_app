require 'test_helper'

class ProductTest < ActiveSupport::TestCase

  def setup
    @product = products(:collar)
  end

  test "should be valid" do
    assert @product.valid?
  end

  # Test Presence

  test "Attributes should be present" do
    required_attrs = ["name", "description", "size"]
    required_attrs.each do |attribute|
  		original_value = @product[attribute]
  		@product[attribute] = "     "
  		assert_not @product.valid?, "Attribute: #{attribute} shuold be present"
  		@product[attribute] = original_value
  	end
  end

  test "Attributes could be blank" do
    blank_attrs = ["code", "brand", "model", "line"]
    blank_attrs.each do |attribute|
  		original_value = @product[attribute]
  		@product[attribute] = "     "
  		assert @product.valid?, "Attribute: #{attribute} could be blank"
      @product[attribute] = nil
      assert @product.valid?, "Attribute: #{attribute} could be nil"
  		@product[attribute] = original_value
  	end
  end

  # Test lenght

  test "Attributes should not be too long" do
  	attrs_lenght = { code: 41, name: 101, description: 256, size: 21,
  									 brand: 101, model: 101, line: 101 }
  	attrs_lenght.each do |attribute, lenght|
  		original_value = @product[attribute]
  		@product[attribute] = "a" * lenght
  		assert_not @product.valid?, "Attribute: #{attribute} should not be greater than: #{lenght-1}"
      @product[attribute] = "a" * (lenght-1)
      assert @product.valid?, "Attribute: #{attribute} should be lower than or equal to: #{lenght}"
  		@product[attribute] = original_value
  	end
	end

  # Test Unique

  test "Attributes should be unique" do
  	product       = products(:prod_1)
  	other_product = products(:prod_2)
  	product.save

  	unique_attrs = [["code"], ["model"], ["name", "size"]]

  	unique_attrs.each do |attribute|
  		attribute.each do |attr| 
  			@original_value = { attr => other_product[attr] }
  			other_product[attr] = product[attr]
  		end
  		assert_not other_product.valid?, "Attribute: #{attribute} should be unique"
  		attribute.each { |attr| other_product[attr] = @original_value[attr] }
  	end
  end

  test "Attributes should not be unique" do
    product       = products(:prod_1)
    other_product = products(:prod_2)
    product.save

    unique_attrs = ["name", "description", "size", "brand", "line"]

    unique_attrs.each do |attribute|
      @original_value = other_product[attribute]
      other_product[attribute] = product[attribute]
      assert other_product.valid?, "Attribute: #{attribute} should not be unique"
      other_product[attribute] = @original_value
    end
  end

  # Test Format

end
