require 'test_helper'

class ProviderTest < ActiveSupport::TestCase

  def setup
    @provider = providers(:lupi)
  end

  test "should be valid" do
    assert @provider.valid?
  end

  # Test Presence

  test "name should be present" do
    @provider.name = "     "
    assert_not @provider.valid?
  end

  # Test lenght

  test "fields should not be too long" do
  	attrs_lenght = { name: 51, tax_name: 101, tax_id: 31, address1: 51,
  									 address2: 51, city: 51, state: 51, zip: 6, country: 51,
  									 contact: 51, mobile: 31, phone: 31, email: 256,
  									 webpage: 256 }
  	attrs_lenght.each do |key, value|
  		original_value = @provider[key]
  		@provider[key] = "a" * value
  		assert_not @provider.valid?, "Valid Provider with #{key} lenght of: #{value}"
  		@provider[key] = original_value
  	end
	end

  # Test Unique

  test "this fields should be unique" do
  	provider       = providers(:prov_1)
  	other_provider = providers(:prov_2)
  	provider.save

  	unique_attrs = ["name", "tax_name", "tax_id", "contact",
  									"mobile", "phone", "email", "webpage"]

  	unique_attrs.each do |field|
  		original_value = other_provider[field]
  		other_provider[field] = provider[field]
  		assert_not other_provider.valid?, "Valid Provider with #{field} repeated"
  		other_provider[field] = original_value
  	end
  end

  # Test Format

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @provider.email = valid_address
      assert @provider.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
	  invalid_addresses.each do |invalid_address|
	    @provider.email = invalid_address
	    assert_not @provider.valid?, "#{invalid_address.inspect} should be invalid"
	  end
	end
end
