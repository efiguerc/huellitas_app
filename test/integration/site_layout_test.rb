require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links with NO logged in user" do
    get root_path
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", contact_path
  end

  test "layout links with logged in user" do
    user = users(:michael)
    log_in_as(user)
    get root_path
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(user)
    assert_select "a[href=?]", edit_user_path(user)
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", contact_path
  end  
end
