require 'test_helper'

class ProvidersNewTest < ActionDispatch::IntegrationTest
  
  test "invalid Provider information" do
  	# provider = providers(:prov_1)
    @user = users(:michael)
    log_in_as(@user)
    get new_provider_path
    assert_no_difference 'Provider.count' do
      post providers_path, provider: { name:  "",
                               				 email: "user@invalid",
                               				 city:  "foo",
                               				 state: "bar" }
    end
    assert_template 'providers/new'
  end
end
