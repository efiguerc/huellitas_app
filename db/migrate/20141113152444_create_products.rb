class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :code
      t.string :name
      t.string :description
      t.string :size
      t.string :brand
      t.string :model
      t.string :line

      t.timestamps null: false
    end
  end
end
