class CreateAgainOrderDetailsSources < ActiveRecord::Migration
  def change
    create_table :order_detail_sources, id: false do |t|
      t.integer :quantity
      t.references :product, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true
      t.references :purchase, index: true, foreign_key: true

      t.index [:product_id, :order_id, :purchase_id], unique: true, name: "by_pr_or_pu"
      t.index [:product_id, :purchase_id, :order_id], unique: true, name: "by_pr_pu_or"
      t.index [:order_id, :product_id, :purchase_id], unique: true, name: "by_or_pr_pu"
      t.index [:order_id, :purchase_id, :product_id], unique: true, name: "by_or_pu_pr"
      t.index [:purchase_id, :product_id, :order_id], unique: true, name: "by_pu_pr_or"
      t.index [:purchase_id, :order_id, :product_id], unique: true, name: "by_pu_or_pr"

      t.timestamps null: false
    end
  end
end
