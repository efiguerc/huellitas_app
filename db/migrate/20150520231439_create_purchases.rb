class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.string :folio
      t.datetime :purchased_at
      t.decimal :cost
      t.references :provider, index: true
    end
  end
end
