class AddItemsArrticlesDiffToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :items, :integer
    add_column :purchases, :articles, :integer
    add_column :purchases, :difference, :decimal
  end
end
