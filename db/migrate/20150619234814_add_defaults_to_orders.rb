class AddDefaultsToOrders < ActiveRecord::Migration
  def change
    change_table :orders do |t|
      t.change_default :o_providers, 0
      t.change_default :o_purchases, 0
      t.change_default :items, 0
      t.change_default :articles, 0
      t.change_default :cost, 0
      t.change_default :margin, 0
      t.change_default :percent, 0
    end
  end
end
