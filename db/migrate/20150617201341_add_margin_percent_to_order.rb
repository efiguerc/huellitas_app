class AddMarginPercentToOrder < ActiveRecord::Migration
  def change
    change_table :orders do |t|
      t.rename :price, :margin
      t.column :percent, :decimal
    end
  end
end
