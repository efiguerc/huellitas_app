class AddDeafultsToPurchasesCostAndDifference < ActiveRecord::Migration
  def change
    change_table :purchases do |t|
      t.change_default :cost, 0
      t.change_default :items, 0
      t.change_default :articles, 0
      t.change_default :difference, 0
    end
  end
end
