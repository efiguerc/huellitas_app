class AddDefaultsToProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.change_default :quantity, 0
      t.change_default :inventory, 0
    end
  end
end
