class CreateAgainOrderDetails < ActiveRecord::Migration
  def change
    create_table :order_details, id: false, force: true do |t|
      t.integer :quantity
      t.decimal :price
      t.references :product, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true
      t.index [:order_id, :product_id], unique: true
      t.index [:product_id, :order_id], unique: true

      t.timestamps null: false
    end
  end
end
