class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.string :name
      t.string :tax_name
      t.string :tax_id
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :contact
      t.string :mobile
      t.string :phone
      t.string :email
      t.string :webpage

      t.timestamps null: false
    end
  end
end
