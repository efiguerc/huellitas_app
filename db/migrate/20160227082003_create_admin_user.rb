class CreateAdminUser < ActiveRecord::Migration
  def up
    admin = User.new( name: "Huellitas App Administrator",
                      email: "admin@huell-itas.com",
                      password: "changeme123.",
                      password_confirmation: "changeme123.",
                      admin: true,
                      activated: true )
    admin.save!
  end

  def down
    admin = User.find_by( email: "admin@huell-itas.com" )
    admin.destroy!
  end
end
