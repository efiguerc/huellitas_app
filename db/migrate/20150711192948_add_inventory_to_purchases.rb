class AddInventoryToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :inventory, :integer
  end
end
