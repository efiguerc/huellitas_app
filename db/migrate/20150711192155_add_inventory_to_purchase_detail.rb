class AddInventoryToPurchaseDetail < ActiveRecord::Migration
  def change
    add_column :purchase_details, :inventory, :integer
  end
end
