class AddPointsToOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :percent
    add_column :orders, :points, :integer
  end
end
