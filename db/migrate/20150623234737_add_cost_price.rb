class AddCostPrice < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.column :cost, :decimal
      t.column :price, :decimal
    end
  end
end
