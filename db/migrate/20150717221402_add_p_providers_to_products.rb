class AddPProvidersToProducts < ActiveRecord::Migration
  def change
    add_column :products, :p_providers, :integer, default: 0
  end
end
