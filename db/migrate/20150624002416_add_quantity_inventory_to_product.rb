class AddQuantityInventoryToProduct < ActiveRecord::Migration
  def change
    add_column :products, :quantity, :integer
    add_column :products, :inventory, :integer
  end
end
