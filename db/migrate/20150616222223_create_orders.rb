class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.datetime :ordered_at
      t.integer :o_providers
      t.integer :o_purchases
      t.integer :items
      t.integer :articles
      t.decimal :cost
      t.decimal :price

      t.timestamps null: false
    end
  end
end
