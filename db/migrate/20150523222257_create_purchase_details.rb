class CreatePurchaseDetails < ActiveRecord::Migration
  def change
    create_table :purchase_details, id: false do |t|
      t.integer :quantity
      t.decimal :cost, precision: 8, scale: 2
      t.references :purchase, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.index [:purchase_id, :product_id], unique: true
      t.index [:product_id, :purchase_id], unique: true

      t.timestamps null: false
    end
  end
end
