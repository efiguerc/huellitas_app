# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobarfoobar",
             password_confirmation: "foobarfoobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "passwordpass"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

Provider.create!(name:      "LUPI PEZ",
                 tax_name:  "",
                 tax_id:    "",
                 address1:  "RIO FRIO  No 176, LOC A",
                 address2:  "MAGDALENA MIXHUCA",
                 city:      "VENUSTIANO CARRANZA",
                 state:     "DISTRITO FEDERAL",
                 zip:       "15850",
                 country:   "MÉXICO",
                 contact:   "ANA,DON OSCAR",
                 mobile:    "",
                 phone:     "41950120",
                 email:     "",
                 webpage:   "")

Provider.create!(name:      "RAY PET",
                 tax_name:  "ISMAEL ALVAREZ GUEVARA",
                 tax_id:    "",
                 address1:  "LABRADORES 82 , ESQ. IMPRENTA",
                 address2:  "MORELOS",
                 city:      "VENUSTIANO CARRANZA",
                 state:     "DISTRITO FEDERAL",
                 zip:       "1",
                 country:   "MÉXICO",
                 contact:   "AMIGUITO",
                 mobile:    "",
                 phone:     "26164939",
                 email:     "",
                 webpage:   "")

Provider.create!(name:      "FANTASY PETS",
                 tax_name:  "FANTASY PETS",
                 tax_id:    "",
                 address1:  "PRIVADA COSNTITUCIÓN No 53",
                 address2:  "EL MANTE",
                 city:      "GUADALAJARA",
                 state:     "JALISCO",
                 zip:       "45235",
                 country:   "MÉXICO",
                 contact:   "",
                 mobile:    "",
                 phone:     "(0133) 36925183",
                 email:     "fantasymundo@live.com",
                 webpage:   "www.fantasymundoanimal.com")

99.times do |n|
  name =      "#{Faker::Company.name} #{n}"
  tax_name =  "#{name} S.A. DE C.V. #{n}"
  tax_id =    Faker::Company.duns_number
  address1 =  Faker::Address.street_address
  address2 =  Faker::Address.secondary_address
  city =      Faker::Address.city
  state =     Faker::Address.state
  zip =       Faker::Number.number(5)
  country =   "MÉXICO"
  contact =   Faker::Name.name
  mobile =    Faker::PhoneNumber.phone_number
  phone =     Faker::PhoneNumber.cell_phone
  email =     Faker::Internet.email
  webpage =   "#{n}#{Faker::Internet.domain_name}"
  Provider.create!(name:      name,
                   tax_name:  tax_name,
                   tax_id:    tax_id,
                   address1:  address1,
                   address2:  address2,
                   city:      city,
                   state:     state,
                   zip:       zip,
                   country:   country,
                   contact:   contact,
                   mobile:    mobile,
                   phone:     phone,
                   email:     email,
                   webpage:   webpage)
end


Product.create!(code:         "R15458-11-18",
                name:         "MANO DE CHANGO",
                description:  "MANO DE CHANGO ROMPENUDOS MANGO ROJO CHINO",
                size:         "U",
                brand:        "CHINO",
                model:        "",
                line:         "N/A",
                cost:        "12.35")

Product.create!(code:         "DG068",
                name:         "ELEGANCE CER M",
                description:  "COLLAR ELEGANCE ROJO M",
                size:         "M",
                brand:        "DOGNESS",
                model:        "",
                line:         "ELEGANCE",
                cost:         "43.75")

Product.create!(code:         "HD91243",
                name:         "DOGI LE SALON PEINE ANTIPULGAS",
                description:  "PEINE CH ROJO CON CERDAS CERRADAS METAL LE SALON ESSENTIALS",
                size:         "U",
                brand:        "LE SALON",
                model:        "",
                line:         "N/A",
                cost:         "34.68")

99.times do |n|
  code =        Faker::Company.duns_number
  name =        Faker::Commerce.product_name
  description = Faker::Lorem.sentence
  size =        Faker::Number.number(2)
  brand =       Faker::Company.name
  model =       "#{Faker::Hacker.noun} #{n}"
  line =        Faker::Commerce.department
  cost =        Faker::Commerce.price
  Product.create!(code:        code,
                  name:        name,
                  description: description,
                  size:        size,
                  brand:       brand,
                  model:       model,
                  line:        line,
                  cost:        cost)
end