# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160227082003) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "adminpack"

  create_table "order_detail_sources", id: false, force: :cascade do |t|
    t.integer  "quantity"
    t.integer  "product_id"
    t.integer  "order_id"
    t.integer  "purchase_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "order_detail_sources", ["order_id", "product_id", "purchase_id"], name: "by_or_pr_pu", unique: true, using: :btree
  add_index "order_detail_sources", ["order_id", "purchase_id", "product_id"], name: "by_or_pu_pr", unique: true, using: :btree
  add_index "order_detail_sources", ["order_id"], name: "index_order_detail_sources_on_order_id", using: :btree
  add_index "order_detail_sources", ["product_id", "order_id", "purchase_id"], name: "by_pr_or_pu", unique: true, using: :btree
  add_index "order_detail_sources", ["product_id", "purchase_id", "order_id"], name: "by_pr_pu_or", unique: true, using: :btree
  add_index "order_detail_sources", ["product_id"], name: "index_order_detail_sources_on_product_id", using: :btree
  add_index "order_detail_sources", ["purchase_id", "order_id", "product_id"], name: "by_pu_or_pr", unique: true, using: :btree
  add_index "order_detail_sources", ["purchase_id", "product_id", "order_id"], name: "by_pu_pr_or", unique: true, using: :btree
  add_index "order_detail_sources", ["purchase_id"], name: "index_order_detail_sources_on_purchase_id", using: :btree

  create_table "order_details", id: false, force: :cascade do |t|
    t.integer  "quantity"
    t.decimal  "price"
    t.integer  "product_id"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "order_details", ["order_id", "product_id"], name: "index_order_details_on_order_id_and_product_id", unique: true, using: :btree
  add_index "order_details", ["order_id"], name: "index_order_details_on_order_id", using: :btree
  add_index "order_details", ["product_id", "order_id"], name: "index_order_details_on_product_id_and_order_id", unique: true, using: :btree
  add_index "order_details", ["product_id"], name: "index_order_details_on_product_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.datetime "ordered_at"
    t.integer  "o_providers", default: 0
    t.integer  "o_purchases", default: 0
    t.integer  "items",       default: 0
    t.integer  "articles",    default: 0
    t.decimal  "cost",        default: 0.0
    t.decimal  "margin",      default: 0.0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "points"
  end

  create_table "products", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.string   "description"
    t.string   "size"
    t.string   "brand"
    t.string   "model"
    t.string   "line"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.decimal  "cost"
    t.decimal  "price"
    t.integer  "quantity",    default: 0
    t.integer  "inventory",   default: 0
    t.string   "picture"
    t.integer  "p_providers", default: 0
  end

  create_table "providers", force: :cascade do |t|
    t.string   "name"
    t.string   "tax_name"
    t.string   "tax_id"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.string   "contact"
    t.string   "mobile"
    t.string   "phone"
    t.string   "email"
    t.string   "webpage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purchase_details", id: false, force: :cascade do |t|
    t.integer  "quantity"
    t.decimal  "cost",        precision: 8, scale: 2
    t.integer  "purchase_id"
    t.integer  "product_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "inventory"
  end

  add_index "purchase_details", ["product_id", "purchase_id"], name: "index_purchase_details_on_product_id_and_purchase_id", unique: true, using: :btree
  add_index "purchase_details", ["product_id"], name: "index_purchase_details_on_product_id", using: :btree
  add_index "purchase_details", ["purchase_id", "product_id"], name: "index_purchase_details_on_purchase_id_and_product_id", unique: true, using: :btree
  add_index "purchase_details", ["purchase_id"], name: "index_purchase_details_on_purchase_id", using: :btree

  create_table "purchases", force: :cascade do |t|
    t.string   "folio"
    t.datetime "purchased_at"
    t.decimal  "cost",         default: 0.0
    t.integer  "provider_id"
    t.integer  "items",        default: 0
    t.integer  "articles",     default: 0
    t.decimal  "difference",   default: 0.0
    t.integer  "inventory"
  end

  add_index "purchases", ["provider_id"], name: "index_purchases_on_provider_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "remember_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  add_foreign_key "order_detail_sources", "orders"
  add_foreign_key "order_detail_sources", "products"
  add_foreign_key "order_detail_sources", "purchases"
  add_foreign_key "order_details", "orders"
  add_foreign_key "order_details", "products"
  add_foreign_key "purchase_details", "products"
  add_foreign_key "purchase_details", "purchases"
end
